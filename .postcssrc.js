// https://github.com/michael-ciniawsky/postcss-load-config
// 注释部分为了用rem转换rpx用的
// const replaceTagSelectorMap = require('postcss-mpvue-wxss/lib/wxmlTagMap')

// const optopns = {
//   cleanSelector: ['*'],
//   remToRpx: 100,
//   replaceTagSelector: Object.assign(replaceTagSelectorMap, {
//     '*': 'view, text' // 将覆盖前面的 * 选择器被清理规则
//   })
// }

module.exports = {
  "plugins": {
    "postcss-mpvue-wxss": {},
    'postcss-salad': {
      browsers: [
        "> 1%",
        "last 2 versions",
        "not ie <= 8"
      ],
      features: {
        bem: {
          shortcuts: { component: 'c', modifier: 'm', descendent: 'd' },
          separators: { modifier: '--', descendent: '__' }
        },
        autoprefixer: false
      }
    },
    cssnano: {
      reduceIdents: false,
      zindex: false
    },
    'postcss-import': {
      addDependencyTo: require('webpack')
    }
  }
}
