export const goodTypeData = [
	{key: '0', name: '不限'},
	{key: '1', name: '房产'},
	{key: '2', name: '机动车'},
	{key: '4', name: '土地'},
	{key: '5', name: '船舶'},
	{key: '6', name: '资产'},
	{key: '128', name: '无形资产'},
	{key: '129', name: '债权'},
	{key: '130', name: '股权'},
	{key: '131', name: '林权'},
	{key: '132', name: '矿权'},
	{key: '133', name: '著作权'}
]

export const priceData = [
	{key: 'key1', name: '10万以下', minPriceStart: 0, minPriceEnd: 100000},
	{key: 'key2', name: '10-50万', minPriceStart: 100000, minPriceEnd: 500000},
	{key: 'key3', name: '50-100万', minPriceStart: 500000, minPriceEnd: 1000000},
	{key: 'key4', name: '100-200万', minPriceStart: 1000000, minPriceEnd: 2000000},
	{key: 'key5', name: '200-400万', minPriceStart: 2000000, minPriceEnd: 4000000},
	{key: 'key6', name: '400-600万', minPriceStart: 4000000, minPriceEnd: 6000000},
	{key: 'key7', name: '600-800万', minPriceStart: 6000000, minPriceEnd: 8000000},
	{key: 'key8', name: '800-1000万', minPriceStart: 4000000, minPriceEnd: 10000000},
	{key: 'key9', name: '1000万以上', minPriceStart: 10000000, minPriceEnd: null}
]

export const getStateText = (state) => {
	let stateText = ''
	switch (state) {
	case 1:
		stateText = '看样排期中'
		break
	case 2:
		stateText = '预约看样'
		break
	case 3:
		stateText = '不安排看样'
		break
	case 4:
		stateText = '预约已结束'
		break
	case 5:
		stateText = '已结束'
		break
	case 6:
		stateText = '流拍'
		break
	case 7:
		stateText = '中止'
		break
	case 8:
		stateText = '撤回'
		break
	default:
		break
	}
	return stateText
}
