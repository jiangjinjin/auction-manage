import {postFetch, getFetch, deleteFetch} from '../http/api'
import wxApi from '../fetchData/wxApi'
import tips from '../utils/tipUtil/tips'
import store from '../store/store'

// 展唐登录接口
export const loginApi = async (params = {}) => {
	const result = await postFetch({
		url: '/apij/member/getToken',
		data: params
	})

	return result
}

export const getLoginVerifyCode = async (params = {}) => {
	const {
		telNo
	} = params
	const result = await postFetch({
		url: '/apij/member/createLoginSmsCode',
		data: {
			telNo
		}
	})

	return result
}
export const getGoodsDetails = async (params = {}) => {
	const result = await getFetch({
		url: '/api/member/findMine',
		data: params
	})

	return result
}
// 微信 code ,sessionkey,openid 换取
export const getWxLoginData = async (params = {}) => {
	const result = await getFetch({
		url: '/apij/wx/user/wx3ca04c4cbb30556e/login',
		data: params
	}).catch(async (res) => {
		wxApi.login2()
		if (res === '服务异常') {
			tips.toast({
				title: '登录状态异常，请重新登录'
			})
		}
	})

	return result
}
// 微信 手机号换取
export const getWxPhone = async (params = {}) => {
	const result = await getFetch({
		url: '/apij/wx/user/wx3ca04c4cbb30556e/phone',
		data: params
	}).catch(async (res) => {
		wxApi.login2()
		store.commit('WX_BASIC_INFO', {
			'isUseGetInfo': false
		})
	  if (res === '服务异常') {
			tips.toast({
				title: '登录状态异常，请重新登录'
			})
		}
	})

	return result
}
// 微信 手机号换取
export const getWxInfo = async (params = {}) => {
	const result = await getFetch({
		url: '/apij/wx/user/wx3ca04c4cbb30556e/info',
		data: params
	})

	return result
}
// 个人预约信息
export const postRecordInfo = async (params = {}) => {
	const result = await postFetch({
		url: '/api/cmsOrders/findPageMa',
		data: params
	})

	return result
}
// 获取个人信息
export const getPersonalInfo = async () => {
	const result = await getFetch({
		url: '/api/member/findMine'
	})

	return result
}

// 提交个人信息

export const setPersonalInfo = async (params = {}) => {
	const result = await postFetch({
		url: '/api/member/updateByMember',
		data: params
	})

	return result
}

// 删除预约地址
// export const deleteRecord = async (id) => {
//   const result = await deleteFetch({
//     url: '/api/cmsOrders/deleteByMember/'+id
//   })
//
//   return result
// }

export const deleteRecord = async (params = {}) => {
	const {
		id
	} = params
	const result = await deleteFetch({
		url: `/api/cmsOrders/deleteByMember/${id}`
	})

	return result
}
export default {}
