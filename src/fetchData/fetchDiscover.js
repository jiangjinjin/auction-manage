import {postFetch, getFetch} from '../http/api'

export const getSearchListApi = async (params = {}) => {
	const {
		goodsName = '',
		province = '',
		city = '',
		region = '',
		goodsTypeId = null,
		// state = null,
		// stateIn = '', 小程序这两个字段不用传
		courtId = '', // 法院id
		sorter = 'auction_time DESC',
		minPriceStart = '', // 起拍价-最小取值
		minPriceEnd = '', // 起拍价-最大取值
		pageSize = 20,
		currentPage = '1'
	} = params
	const result = await postFetch({
		url: '/apij/goods/findPage',
		data: {
			goodsName,
			province,
			city,
			region,
			goodsTypeId,
			sorter,
			minPriceStart,
			minPriceEnd,
			courtId,
			pageSize,
			currentPage
		}
	})

	return result.list
}

export const getGoodsDetails = async (params = {}) => {
	const {
		goodsId
	} = params
	const result = await getFetch({
		url: `/apij/goods/findOneDetailApp/${goodsId}`
	})

	return result.data
}

export const yuyueRegisterApi = async (params = {}) => {
	const {
		goodsId,
		goodsDateId,
		man,
		tel
	} = params

	const result = await postFetch({
		url: '/apij/cmsOrders/createFromMa',
		data: {
			goodsId,
			goodsDateId,
			man,
			tel
		}
	})

	return result
}

export const isYuYueRegisterApi = async (params = {}) => {
	const {
		tel,
		goodsId
	} = params

	const result = await postFetch({
		url: '/apij/cmsOrders/findPage',
		data: {
			tel,
			goodsId
		}
	})

	return result.list
}

export const getCourtCityApi = async (params = {}) => {
	const result = await getFetch({
		url: '/apij/court/findCourtCity',
		data: params
	})

	return result.list
}

export const getCourtAllApi = async (params = {}) => {
	const {
		cityId
	} = params

	const result = await postFetch({
		url: '/apij/court/findAll',
		data: {
			cityId
		}
	})

	return result.list
}

export default {}
