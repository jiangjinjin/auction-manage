import {getWxLoginData, getWxPhone, loginApi, getPersonalInfo} from './fetchMyProfile'
import store from '../store/store'
import {apiDeps} from '../http/api'

const wxApi = {
	login: () => {
		// 验证登录
		wx.checkSession({
			success: function () {
			  console.log('微信自动登录成功')
				if (store.state.wxBasicInfo.openId) {
					// 网站登陆判断，提供后加入
					// store.commit('WX_BASIC_INFO', {
					//   'isLogin': true,
					// })
					isZTlogin()
				} else {
					store.commit('WX_BASIC_INFO', {
						'isUseGetInfo': false
					})
					login()
				}
			},
			fail: function () {
				console.log('微信自动登录失败')
				login()
			}
		})
	},
	getPhone: async ({encryptedData, iv} = {}) => {
		let value = await getWxPhone({
			appid: 'wx3ca04c4cbb30556e',
			sessionKey: store.state.wxBasicInfo.sessionKey,
			encryptedData: encryptedData,
			iv: iv
		})
		store.commit('WX_BASIC_INFO', {
			'phone': value.data.purePhoneNumber,
			'isLogin': true
		})
		let value2 = await ztLogin()
		return value2
	},
	login2: () => {
		console.log('登录111')
		wx.login({
			success: (res) => {
				code = res.code
				console.log('2222')
				postCode()
			},
			fail: (res) => {
				console.log('3333')
				console.log(res)
			}
		})
	}
}

let code = ''
const login = () => {
	console.log('登录111')
	wx.login({
		success: (res) => {
			code = res.code
			console.log('2222')
			postCode()
		},
		fail: (res) => {
			console.log('3333')
			console.log(res)
		}
	})
}
const postCode = async () => {
	console.log('444')
	let value = await getWxLoginData({
		code: code
	})
	console.log(store.state.wxBasicInfo)
	console.log(store.state.wxBasicInfo.sessionKey)
	store.commit('WX_BASIC_INFO', value.data)
	console.log(store.state.wxBasicInfo.sessionKey)
}
const ztLogin = async () => {
	let data = store.state.wxBasicInfo
	let value = await loginApi({
		type: 2,
		tel: data.phone,
		unionId: data.openid
	})
	store.commit('WX_BASIC_INFO', {
		digest: value.data.digest,
		telNo: value.data.telNo,
		uid: value.data.uid
	})
	isZTlogin()
	let value2 = await getPersonalInfo()

	store.commit('WX_BASIC_INFO', {
		realName: value2.data.realName
	})
	if (value.code === '100') {
		return true
	}
}
const isZTlogin = () => {
	if (!store.state.wxBasicInfo.digest) {
		return false
	}
	apiDeps['Access-Token'] = store.state.wxBasicInfo.digest + ',' + store.state.wxBasicInfo.telNo + ',' + store.state.wxBasicInfo.uid + ',2'
}
export default wxApi
