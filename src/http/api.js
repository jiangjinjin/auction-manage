/*
    接口配调用文件
*/
import fly from './config'
// import qs from 'qs'

export const apiDeps = {
	'Access-Token': ''
}

function getDefaultHeader () {
	return {
		'Access-Token': apiDeps['Access-Token']
	}
}

function requestBodyConfigs (options = {}) {
	return {
		...fly.config,
		...options,
		headers: {
			...fly.config.headers,
			...getDefaultHeader(),
			...options.headers || {}
		}
	}
}

/**
 * get 请求方法
 */

export const getFetch = async (params) => {
	const {
		url,
		data = {},
		options = {}
	} = params

	const newOptions = {
		...requestBodyConfigs(options)
	}

	return new Promise((resolve, reject) => {
		fly.get(url, data, newOptions)
			.then(response => {
				resolve(response)
			}, err => {
				reject(err)
			})
	})
}

/**
 * post请求方法
 */

export const postFetch = async (params) => {
	const {
		url,
		data = {},
		options = {}
	} = params

	const newOptions = {
		...requestBodyConfigs(options)
	}

	// const newData = qs.stringify(data)

	return new Promise((resolve, reject) => {
		fly.post(url, data, newOptions)
			.then(response => {
				resolve(response)
			}, err => {
				reject(err)
			})
	})
}

/**
 * delete请求方法
 */

export const deleteFetch = async (params) => {
	const {
		url,
		data = {},
		options = {}
	} = params

	const newOptions = {
		...requestBodyConfigs(options)
	}

	return new Promise((resolve, reject) => {
		fly.delete(url, data, newOptions)
			.then(response => {
				resolve(response)
			}, err => {
				reject(err)
			})
	})
}

/**
 * 上传文件的formdata请求
 */

export const uploadFileFetch = async (params) => {
	const {
		url,
		data = {},
		options = {
			headers: {
				'Content-Type': 'multipart/form-data'
			}
		}
	} = params

	const newOptions = {
		...requestBodyConfigs(options),
		method: 'post'
	}

	return new Promise((resolve, reject) => {
		fly.request(url, data, newOptions)
			.then(response => {
				resolve(response)
			}, err => {
				reject(err)
			})
	})
}
