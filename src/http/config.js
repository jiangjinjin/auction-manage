/*
    fly中文文档地址https://wendux.github.io/dist/#/doc/flyio/readme
*/

import {host} from '../config/apiconfig/config'
import Tips from '../utils/tipUtil/tips'

const Fly = require('flyio/dist/npm/wx')
const fly = new Fly()

// 实例级配置请求配置
fly.config = {
	...fly.config,
	timeout: 30000, // 超时时间
	headers: {
		...fly.config.headers,
		'Content-Type': 'application/json;charset=UTF-8'
	},
	baseURL: host	// 请求基地址
}

// 添加请求拦截器
fly.interceptors.request.use((request) => {
	// 给所有请求添加自定义header
	request.headers['X-Tag'] = 'flyio'
	// wx.showNavigationBarLoading()
	Tips.loading()
	console.log('request:', request)
	return request
})

// 添加响应拦截器，响应拦截器会在then/catch处理之前执行
fly.interceptors.response.use(
	(response) => {
		// 只将请求结果的data字段返回
		// wx.hideNavigationBarLoading()
		Tips.loaded()
		// code为100默认为成功
		console.log('response:', response.data)
		if (response.data.code === '100') {
			return response.data
		} else if (response.data.code === '107') {
			this.$store.commit('Login', false)
		  wx.navigateTo({
				url: '../pages/login/main',
				success: function () {
					Tips.toast({title: '登录状态过期'})
				}
			})
		} else {
			Tips.toast({title: response.data.msg})

			return Promise.reject(response.data.msg)
		}
	},
	(err, promise) => {
		// 发生网络错误后会走到这里
		// wx.hideNavigationBarLoading()
		Tips.toast({title: `网络请求异常${err.message}`})
		return promise.reject()
	}
)

export default fly
