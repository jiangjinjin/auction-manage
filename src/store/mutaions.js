import {
	WX_BASIC_INFO,
	RECORD_INFO
} from './mutation-type'

const mutations = {
	[WX_BASIC_INFO] (state, data) {
		state.wxBasicInfo = {
			...state.wxBasicInfo,
			...data
		}
	},
	[RECORD_INFO] (state, data) {
		state.recordInfo = data
	}
}
export default mutations
