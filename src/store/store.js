import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import mutations from './mutaions'
import getters from './getters'
import actions from './actions'

Vue.use(Vuex)

const state = {
	wxBasicInfo: {
		isLogin: false,
		isUseGetInfo: false,
		openid: '', // 微信对应公众平台唯一ID
		unionid: '', // 公众平台关联ID
		sessionKey: '', // 登录换取参数，用于验证
		phone: '', // 手机号
		realName: '', // 姓名
		digest: ''// 展唐验证码
	}, // 小程序基础信息
	recordInfo: []
}

const store = new Vuex.Store({
	state,
	getters,
	mutations,
	actions,
	modules: {
	},
	plugins: [createPersistedState({
		// 配置白名单
		paths: [
			'wxBasicInfo'
		],
		storage: {
			getItem: key => wx.getStorageSync(key),
			setItem: (key, value) => wx.setStorageSync(key, value),
			removeItem: key => {}
		}
	})]
})

export default store
