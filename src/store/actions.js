import { getWxLoginData } from '../fetchData/fetchMyProfile'

export default {
	async getLoginBasicInfo ({commit, state}, data) { // 前面对象是vuex返回自带的，该对象里有commit,state等参数，具体自己看vuex文档，data作为自己传的参数，
		const result = await getWxLoginData()
		console.log(result)
		commit('WX_BASIC_INFO', result)
	}
}
