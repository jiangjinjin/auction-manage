import Vue from 'vue'
import App from './App'
import _ from 'lodash'
import '../static/weui/weui.css'
import './utils/style/common.css'
import store from './store/store'
import Tips from './utils/tipUtil/tips'

Vue.config.productionTip = false
App.mpType = 'app'
Vue.prototype.$store = store
Vue.prototype.$tips = Tips
Vue.prototype.$_ = _

const app = new Vue(App)
app.$mount()
